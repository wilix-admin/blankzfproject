<?php
return array(
	'db' => array(
		'driver'         => 'Pdo',
		'dsn'            => 'mysql:dbname=servison;host=localhost',
		'driver_options' => array(
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'',
		),
	),
	'service_manager' => array(
		'factories' => array(
			'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
			'logger' => function ($sm) {
				$logger = new Zend\Log\Logger();
				$writer = new Zend\Log\Writer\Stream(APP_PATH . '/data/logs/app.log');
				$formatter = new Zend\Log\Formatter\Simple(
					'%timestamp% %priorityName% (%priority%): %message%',
					'd.m.Y H:i:s');
				$writer->setFormatter($formatter);
				$logger->addWriter($writer);
				
				// Capture PHP errors
				\Zend\Log\Logger::registerErrorHandler($logger, true);
				\Zend\Log\Logger::registerExceptionHandler($logger);
				
				return $logger;
			},
			'Zend\Session\SessionManager' => function ($sm){
				$config = new \Zend\Session\Config\StandardConfig();
				$config->setOptions(array(
						'name' => 'App',
						'save_path' => APP_PATH . '/data/sessions',
						'use_cookies' => true,
						'cookie_lifetime' => 3024000,
						'gc_maxlifetime' => 3024000,
						'remember_me_seconds' => 3024000
				));
				$sessionManager = new \Zend\Session\SessionManager($config);
				//TODO I am hardcode!
				session_save_path(APP_PATH . '/data/sessions');
				return $sessionManager;
			},
			'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
// 			'mail_transport' => function ($sm){
// 				$options = [
// 					'name'              => 'gmail',
// 					'host'              => 'smtp.gmail.com',
// 					'port'              => 587,
// 					'connection_class'  => 'login',
// 						'connection_config' => array(
// 							'username' => 'root@servison.ru',
// 							'password' => 'zSG366oy22',
// 							'ssl' => 'tls'
// 						),
// 				];
// 				$transport = new \Zend\Mail\Transport\Smtp();
// 				$transport->setOptions(new \Zend\Mail\Transport\SmtpOptions($options));
// 				return $transport;
// 			},
// 			'mail_message' => function($sm){
// 				$message = new \Zend\Mail\Message();
// 				$message->addFrom('support@servison.ru', 'Servison')
// 						->setEncoding('UTF-8');
// 				return $message;
// 			},
		),
	),
// 	'translator' => array(
// 		'locale' => 'ru_RU',
// 		'translation_file_patterns' => array(
// 			array(
// 				'type'     => 'gettext',
// 				'base_dir' => __DIR__ . '/../../language',
// 				'pattern'  => '%s.mo',
// 			),
// 		),
// 	),
	'navigation' => array(
		'default' => array(
			array(
				'label' => 'Главная',
				'route' => 'home',
				'i' => 'glyphicon glyphicon-home',
				'icon_space' => '',
			),
		),
	),
	
	'images' => array(
		'full_width' => 300,
		'full_height' => 300,

		'thumb_width' => 170,
		'thumb_height' => 170,
		
		'small_width' => 100,
		'small_height' => 100,
	),
	
	'sms_gate' => array(
		'email' => 'joviallix@gmail.com',
		'password' => 'azsxdc',
		'sender_name' => 'Servison.ru'
	),
);
