<?php
namespace App\Model;

abstract class DbObject extends Dbo {
    const NULL_DATE = '0000-00-00 00:00:00';
    private $_TABLE = '';
    private $_PRIMARY = '';

    public function __construct($bind = null){
        if(! empty( $bind )){
            if(is_array( $bind ) || is_object( $bind ))
                $this->bindTo( $bind );
            elseif(is_numeric( $bind ) || is_string( $bind )){
                $ob = $this->loadFromDb( $bind );
                if(! empty( $ob ))
                    $this->bindTo( $ob );
            }
        }
        return $this;
    }

    /**
     * Return true if primary id not 0
     * 
     * @return boolean
     */
    public function isNotEmpty(){
        if(isset( $this->{$this->getPrimary()} ) && ! empty( $this->{$this->getPrimary()} ))
            return true;
        return false;
    }

    /**
     * Load object from DB where primary id
     * 
     * @param number|string $id            
     * @return boolean
     */
    public function loadFromDb($id){
        if(empty( $id ))
            return false;
        
        $select = self::getSql()->select( $this->_TABLE );
        $select->columns( [
            '*' 
        ] )->where( [
            $this->_PRIMARY => $id 
        ] )->limit( 1 );
        
        $res = self::q( self::getSql()->getSqlStringForSqlObject( $select ) );
        if(! $res || ! $res->count())
            return false;
        return $res->current();
    }

    /**
     * Return primary name
     * 
     * @return string
     */
    public function getPrimary(){
        return $this->_PRIMARY;
    }

    /**
     * Return primary ID
     */
    public function getPrimatyId(){
        if($this->{$this->getPrimary()} instanceof \Zend\Db\Sql\Expression)
            return $this->{$this->getPrimary()};
        
        if(isset( $this->{$this->getPrimary()} ) && $this->{$this->getPrimary()} > 0)
            return $this->{$this->getPrimary()};
        return false;
    }

    /**
     * Bind data to object property
     * 
     * @param array|object $bind            
     * @param boolean $bindAll
     *            - in need bind all input data
     * @return boolean|mixed
     */
    public function bindTo($bind, $bindAll = false){
        if(empty( $bind ))
            return false;
        
        foreach( $bind as $key => $val ){
            if(substr( $key, 0, 10 ) == 'idatetime_'){
                $val = date( 'Y-m-d H:i:s', strtotime( $val ) );
                $key = substr( $key, 10 );
            }
            if(substr( $key, 0, 6 ) == 'idate_'){
                $val = date( 'Y-m-d', strtotime( $val ) );
                $key = substr( $key, 6 );
            }
            if(! $bindAll){
                if(property_exists( $this, $key ))
                    $this->$key = $val;
            }else{
                $this->$key = $val;
            }
        }
        return $this;
    }

    /**
     * Store self data
     * 
     * @param boolean $principial_insert
     *            - USE ONLY INSERT
     * @return number|string;
     */
    public function save($principial_insert = false){
        if(empty( $this->_PRIMARY ) || empty( $this->_TABLE )){
            Sm::log( 'В классе не указана таблица или primary_id.' );
            return false;
        }
        
        $primary = $this->getPrimary();
        $res = parent::store( $this->_TABLE, $this, $primary, $principial_insert );
        
        if($res == false){
            Sm::log( 'Не удалось сохранить объект!' );
            return false;
        }
        
        $primary_id = $this->getPrimatyId();
        
        if($primary_id instanceof \Zend\Db\Sql\Expression){
            if(! empty( $res ))
                $this->$primary = $res;
            else
                return $res;
        }
        
        if((! empty( $primary_id ) && ! empty( $res )) || (empty( $primary_id ) && ! empty( $res )))
            $this->$primary = $res;
        
        return $res;
    }

    /**
     * Remove row from db
     * 
     * @return boolean
     */
    public function remove(){
        $delete = self::getSql()->delete( $this->_TABLE );
        $delete->where( [
            $this->_PRIMARY => $this->getPrimatyId() 
        ] );
        $res = self::q( self::getSql()->getSqlStringForSqlObject( $delete ) );
        if(! $res || ! $res->count())
            return false;
        return true;
    }

    /**
     * Clear object.
     * Set All vars to null
     * 
     * @return \App\Model\DbRow
     */
    public function clearObject($value = null){
        $fn = function ($b){
            return get_object_vars( $b );
        };
        $b = $fn( $this );
        foreach( $b as $key => $val )
            if(substr( $key, 0, 1 ) == '_')
                continue;
            else
                $this->$key = $value;
        return $this;
    }
}
