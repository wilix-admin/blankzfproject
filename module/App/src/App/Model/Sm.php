<?php
namespace App\Model;

class Sm
{
	/**
	 * @var \Zend\ServiceManager\ServiceLocatorInterface
	 */
	private static $sm;
	
	/**
	 * @var \Zend\Log\Logger
	 */
	private static $logger;

	/**
	 * @var \Zend\Session\Container
	 */
	private static $sessionDefault;
	
	/**
	 * Set ServiceLocator
	 * @param \Zend\ServiceManager\ServiceLocatorInterface $sm
	 */
	public static function setSm( \Zend\ServiceManager\ServiceLocatorInterface $sm )
	{
		return self::$sm = $sm;
	}
	
	/**
	 * Return ServiceManager or service
	 * @param string $service
	 * @return boolean|\Zend\ServiceManager\ServiceLocatorInterface
	 */
	public static function getSm( $service = '' )
	{
		if( empty(self::$sm) )
			return false;
		
		if( empty($service) )
			return self::$sm;
		
		return self::$sm->get($service);
	}
	
	/**
	 * Log or get logger
	 * @param mixed $msg
	 * @param string $type = info, emerg, alert, crit, err, warn, notice, debug
	 */
	public static function log( $msg = '', $type = 'info' )
	{
		if( empty(self::$logger) )
			self::$logger = self::getSm('logger');
		
		if( !empty($msg) ){
			if( !is_string($msg) && !is_int($msg) ){
				$msg = var_export($msg, 1);
			}
			self::$logger->$type( $msg );
		}
		return self::$logger;
	}

	/**
	 * Add message to session
	 * @param string $msg
	 * @param string $type
	 */
	public static function addMessage( $msg, $type = 'info' )
	{
		if( !isset(self::getSessionContainer()->sysMessages) ){
			self::$sessionDefault->sysMessages = [];
		}
		
		self::$sessionDefault->sysMessages[$type][] = $msg;
	}
	
	/**
	 * Return messages from session
	 * @param string $type
	 * @return multitype:
	 */
	public static function getMessages( $type = false )
	{
		$msgs = isset(self::getSessionContainer()->sysMessages)?self::getSessionContainer()->sysMessages:[];
		if( !$type )
			return $msgs;
		else
			return !empty($msgs[$type])?$msgs[$type]:[];
	}
	
	/**
	 * Clear all messages
	 */
	public static function clearMessages()
	{
		self::getSessionContainer()->sysMessages = [];
	}
	
	/**
	 * Return default session container
	 * @return \Zend\Session\Container
	 */
	public static function getSessionContainer()
	{
		if( empty(self::$sessionDefault) ){
			self::$sessionDefault = new \Zend\Session\Container('Default');
		}
		return self::$sessionDefault;
	}
}
