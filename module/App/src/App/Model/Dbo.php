<?php
namespace App\Model;

class Dbo {
    /**
     * Store data to db.
     * If provided primary value use update or insert
     * 
     * @param string $table            
     * @param array|object $bind            
     * @param string $primary            
     * @param string $update            
     * @return boolean|Ambigous <>|\Zend\Db\Adapter\Driver\mixed
     */
    public static function store($table, $bind, $primary, $principial_insert = false){
        if(empty( $table ) || empty( $bind ))
            return false;
        
        $sql = new \Zend\Db\Sql\Sql( self::getAd(), $table );
        
        if(is_object( $bind )){
            $fn = function ($b){
                return get_object_vars( $b );
            };
            $b = $fn( $bind );
            $bind = array();
            foreach( $b as $key => $val ){
                if(substr( $key, 0, 1 ) == '_')
                    continue;
                $bind[$key] = $val;
            }
        }
        
        if(! empty( $bind[$primary] ) && ! $principial_insert && ! ($bind[$primary] instanceof \Zend\Db\Sql\Expression)){ // Update
            $update = $sql->update();
            $update->set( $bind )->where( array(
                $primary => $bind[$primary] 
            ) );
            $res = self::q( $sql->getSqlStringForSqlObject( $update ) );
            return $bind[$primary];
        }else{ // Insert
            $insert = $sql->insert();
            $insert->values( $bind );
            $res = self::q( $sql->getSqlStringForSqlObject( $insert ) );
            if($res)
                return self::getAd()->getDriver()->getLastGeneratedValue();
            else
                return false;
        }
    }

    /**
     * Simple method for adapter query
     * 
     * @param string $sql
     *            - string SQL query
     * @param string|array $mode
     *            - Array params or Adapter::QUERY_MODE_EXECUTE
     * @return Driver\StatementInterface|ResultSet\ResultSet
     * @throws Exception\InvalidArgumentException
     */
    public static function q($sql, $mode = \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE){
        if(empty( $sql ))
            return false;
        try{
            return self::getAd()->query( $sql, $mode );
        }catch( \PDOException $e ){
            Sm::log( $e->getMessage(), 'err' );
            if($e->getCode() == 23000){
                Sm::addMessage( 'Такая запись уже существует', 'err' );
                Sm::log( 'Такая запись уже существует', 'err' );
                Sm::log( $e->getMessage(), 'err' );
            }else{
                Sm::addMessage( 'Произошла ошибка при обращении к БД', 'err' );
                Sm::log( 'Произошла ошибка при обращении к БД', 'err' );
                Sm::log( $e->getMessage(), 'err' );
            }
            return false;
        }
    }

    /**
     * Return DB Adapter
     * 
     * @return \Zend\Db\Adapter\Adapter
     */
    public static function getAd(){
        return \App\Model\Sm::getSm( 'Zend\Db\Adapter\Adapter' );
    }

    /**
     * Return Sql object
     * 
     * @return \Zend\Db\Sql\Sql
     */
    public static function getSql(){
        $sql = new \Zend\Db\Sql\Sql( self::getAd() );
        return $sql;
    }

    /**
     * Create find_in_set query
     * 
     * @param string|array $data            
     * @param string $field            
     * @param string $and            
     * @return string
     */
    public static function findInSet($data, $field, $and = 'OR'){
        if(empty( $data ) || empty( $field ))
            return '';
        
        if(strpos( $data, ',' ) !== false || is_array( $data )){
            $q = '';
            if(is_array( $data ))
                $segs = $data;
            else
                $segs = explode( ',', $data );
            foreach( $segs as $seg ){
                $q .= ' FIND_IN_SET("' . $seg . '", `' . $field . '`) ' . $and;
            }
            $q = trim( $q, $and );
            return $q . ' ';
        }else
            return ' FIND_IN_SET("' . $data . '", `' . $field . '`) ';
    }
}
