<?php
namespace App;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $sm = $e->getApplication()->getServiceManager();
		$eventManager        = $e->getApplication()->getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);
		
		\App\Model\Sm::setSm($sm);
		
		$session = $sm->get('Zend\Session\SessionManager');
		$session->start();
		\Zend\Session\Container::setDefaultManager($session);
		
		\App\Model\Sm::log();
		
		$viewHelperManager = $sm->get('ViewHelperManager');
		$pluralHelper      = $viewHelperManager->get('Plural');
		$pluralHelper->setPluralRule('nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;');
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
